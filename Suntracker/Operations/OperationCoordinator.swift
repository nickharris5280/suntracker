//
//  OperationCoordinator.swift
//  Suntracker
//
//  Created by Nick Harris on 2/10/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

protocol OperationCoordinatorViewController {
    var operationCoordinator: OperationCoordinator? { get set }
}

class OperationCoordinator {
    
    let operationQueue: NSOperationQueue
    
    init() {
        
        operationQueue = NSOperationQueue()
    }
    
    // MARK: Today Info
    func calculateTodayInfoStrings(coordinate: CLLocationCoordinate2D, completionBlock:(error: NSError?, sunriseTime: String?, sunsetTime: String?, totalDaylight: String?, daylightSavings: String?)->Void) {
        
        // create the standard operations
        let calculateSunriseSunsetOperation = CalculateSunriseSunsetOperation()
        let createTodayInfoStringsOperation = CreateTodayInfoStringsOperation()
        
        // setup the calculateSunriseSunsetOperation with the passed in coordinates
        calculateSunriseSunsetOperation.operationInput = (coordinate: coordinate, date: NSDate())
        
        // pass the caluclated data to the operation that will create and store the UI strings
        let processSunriseSunsetCalculationsOperation = NSBlockOperation {
            [unowned calculateSunriseSunsetOperation, unowned createTodayInfoStringsOperation] in
            
            let calculateSunriseSunsetOperationResult = calculateSunriseSunsetOperation.operationResult
            
            if let error = calculateSunriseSunsetOperationResult.error {
                createTodayInfoStringsOperation.cancel()
                completionBlock(error: error, sunriseTime: nil, sunsetTime: nil, totalDaylight: nil, daylightSavings: nil)
            }
            else {
                createTodayInfoStringsOperation.operationInput = (sunriseTimeInMinutes: calculateSunriseSunsetOperationResult.sunriseTimeInMinutes,
                    sunsetTimeInMinutes: calculateSunriseSunsetOperationResult.sunsetTimeInMinutes)
            }
        }
        
        // once the strings are create, call the completion block
        let processTodayInfoStringsOperation = NSBlockOperation {
            [unowned createTodayInfoStringsOperation] in
            
            let createTodayPanelStringsOperationResult = createTodayInfoStringsOperation.operationResult
            
            if let error = createTodayPanelStringsOperationResult.error {
                completionBlock(error: error, sunriseTime: nil, sunsetTime: nil, totalDaylight: nil, daylightSavings: nil)
            }
            else {
                completionBlock(error: nil,
                    sunriseTime: createTodayPanelStringsOperationResult.sunriseTime,
                    sunsetTime: createTodayPanelStringsOperationResult.sunsetTime,
                    totalDaylight: createTodayPanelStringsOperationResult.totalDaylight,
                    daylightSavings: createTodayPanelStringsOperationResult.daylightSavings)
            }
        }
        
        // set dependencies
        processSunriseSunsetCalculationsOperation.addDependency(calculateSunriseSunsetOperation)
        createTodayInfoStringsOperation.addDependency(processSunriseSunsetCalculationsOperation)
        processTodayInfoStringsOperation.addDependency(createTodayInfoStringsOperation)
        
        // add all the operations to the queue
        operationQueue.addOperation(calculateSunriseSunsetOperation)
        operationQueue.addOperation(processSunriseSunsetCalculationsOperation)
        operationQueue.addOperation(createTodayInfoStringsOperation)
        operationQueue.addOperation(processTodayInfoStringsOperation)
    }
    
    // MARK: Trending Chart
    func createTrendingChart(trendingChartType: TrendingChartType, coordinate: CLLocationCoordinate2D, completionBlock:(error: NSError?, totalSunlightHoursForChart: [TrendingChartData])->Void) {
        
        // figure out how many days back and how many days ahead to calculate
        let daysFromCurrentDate = trendingChartType.daysFromCurrentDate()
        
        // create an array of operations for each day on the chart
        var calculateSunriseSunsetOperations: [CalculateSunriseSunsetOperation] = []
        
        for dayInRange in ((daysFromCurrentDate * -1)...daysFromCurrentDate) {
            
            let calculateSunriseSunsetOperation = CalculateSunriseSunsetOperation()
            calculateSunriseSunsetOperations.append(calculateSunriseSunsetOperation)
            calculateSunriseSunsetOperation.operationInput = (coordinate: coordinate, date: NSDate().newDateByDays(dayInRange))
        }
        
        // create a block operation to loop through all the calculation operations and pass them back in the completion block
        let processResultsOperation = NSBlockOperation() {
            
            let dateFormater = NSDateFormatter()
            dateFormater.dateStyle = .MediumStyle
            
            var totalSunlightHoursForChart: [TrendingChartData] = []
            
            for calculateSunriseSunsetOperation in calculateSunriseSunsetOperations {
                
                let calculateSunriseSunsetOperationInput = calculateSunriseSunsetOperation.operationInput
                let calculateSunriseSunsetOperationResult = calculateSunriseSunsetOperation.operationResult
                
                // make sure we have all the data we need from the calculateSunriseSunsetOperation
                guard let sunriseTimeInMinutes = calculateSunriseSunsetOperationResult.sunriseTimeInMinutes,
                      let sunsetTimeInMinutes = calculateSunriseSunsetOperationResult.sunsetTimeInMinutes,
                    let calculatedDate = calculateSunriseSunsetOperationInput.date else {
                        fatalError("Not enough information to calculate trending chart")
                }
                
                let totalDaylightInMinutes = sunsetTimeInMinutes - sunriseTimeInMinutes
                
                let isDST = NSTimeZone.systemTimeZone().isDaylightSavingTimeForDate(calculatedDate)
                
                let dateString = dateFormater.stringFromDate(calculateSunriseSunsetOperationInput.date!)
                let sunriseTime = String.timeStringFromMinutes(sunriseTimeInMinutes, isDST: isDST)
                let sunsetTime = String.timeStringFromMinutes(sunsetTimeInMinutes, isDST: isDST)
                let totalDaylight = String.totalDaylightString(sunriseTimeInMinutes, sunsetTimeInMinutes: sunsetTimeInMinutes)
                
                let trendingChartData = TrendingChartData(totalMinutes: totalDaylightInMinutes, dateString: dateString, sunriseTime: sunriseTime, sunsetTime: sunsetTime, totalDaylight: totalDaylight)
                totalSunlightHoursForChart.append(trendingChartData)
            }
            
            completionBlock(error: nil, totalSunlightHoursForChart: totalSunlightHoursForChart)
        }
        
        // setup the dependencies
        for calculateSunriseSunsetOperation in calculateSunriseSunsetOperations {
            processResultsOperation.addDependency(calculateSunriseSunsetOperation)
        }
        
        // add the operations to the queue
        operationQueue.addOperation(processResultsOperation)
        operationQueue.addOperations(calculateSunriseSunsetOperations, waitUntilFinished: false)
    }
    
    // MARK: Create location notifications
    func createLocalNotifications(coordinate: CLLocationCoordinate2D, sunriseLocalNotifications: Bool, sunsetLocalNotifications: Bool, application: UIApplication) {
        
        // create the local variables
        var allOperations: [NSOperation] = []
        
        for dayLoopIndex in (0...6) {
            
            // first create the date for this loop interation
            let date = NSDate().newDateByDays(dayLoopIndex)
            
            // create the CalculateSunriseSunsetOperation
            let calculateSunriseSunsetOperation = CalculateSunriseSunsetOperation()
            calculateSunriseSunsetOperation.operationInput = (coordinate: coordinate, date: date)
            allOperations.append(calculateSunriseSunsetOperation)
            
            if sunriseLocalNotifications {
                // create the operation to schedule the sunrise local notification
                let scheduleSunriseLocalNotificationOperation = ScheduleLocalNotificationOperation()
                
                // create the transfer operation
                let transferDataBlockOperation = createTransferDataBlockOperation(calculateSunriseSunsetOperation, scheduleSunriseLocalNotificationOperation: scheduleSunriseLocalNotificationOperation, localNotificationType: LocalNotificationType.Sunrise, application: application)
                
                // add the operation to the array
                allOperations.append(scheduleSunriseLocalNotificationOperation)
                allOperations.append(transferDataBlockOperation)
            }
            
            if sunsetLocalNotifications {
                // create the operation to schedule the sunset local notification
                let scheduleSunsetLocalNotificationOperation = ScheduleLocalNotificationOperation()
                
                // create the transfer operation
                let transferDataBlockOperation = createTransferDataBlockOperation(calculateSunriseSunsetOperation, scheduleSunriseLocalNotificationOperation: scheduleSunsetLocalNotificationOperation, localNotificationType: LocalNotificationType.Sunset, application: application)
                
                // add the operation to the array
                allOperations.append(scheduleSunsetLocalNotificationOperation)
                allOperations.append(transferDataBlockOperation)
            }
        }
        
        // queue all the operations
        operationQueue.addOperations(allOperations, waitUntilFinished: false)
    }
    
    private func createTransferDataBlockOperation(calculateSunriseSunsetOperation: CalculateSunriseSunsetOperation, scheduleSunriseLocalNotificationOperation: ScheduleLocalNotificationOperation, localNotificationType: LocalNotificationType, application: UIApplication) -> NSBlockOperation {
        
        let transferDataBlockOperation = NSBlockOperation() {
            [unowned calculateSunriseSunsetOperation, unowned scheduleSunriseLocalNotificationOperation] in
            
            let operationInput = calculateSunriseSunsetOperation.operationInput
            let operationResult = calculateSunriseSunsetOperation.operationResult
            
            var timeInMinutes: Double?
            switch localNotificationType {
            case .Unknown: fatalError("LocationNotificationType is unknown")
            case .Sunrise: timeInMinutes = operationResult.sunriseTimeInMinutes
            case .Sunset: timeInMinutes = operationResult.sunsetTimeInMinutes
            }
            
            scheduleSunriseLocalNotificationOperation.operationInput = (date: operationInput.date, timeInMinutes: timeInMinutes, localNotificationType: localNotificationType, application: application)
        }
        
        // add the dependency to calculateSunriseSunsetOperation
        transferDataBlockOperation.addDependency(calculateSunriseSunsetOperation)
        scheduleSunriseLocalNotificationOperation.addDependency(transferDataBlockOperation)
        
        return transferDataBlockOperation
    }
}
