//
//  CreateBottomPanelStringsOperation.swift
//  Suntracker
//
//  Created by Nick Harris on 2/16/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit

class CreateTodayInfoStringsOperation: NSOperation {
    
    struct Constants {
        static let errorDomain = "CreateBottomPanelStringsOperationError"
        static let requiredDataMissingErrorCode = 1
    }
    
    // MARK: Public Properties
    var operationInput: (sunriseTimeInMinutes: Double?, sunsetTimeInMinutes: Double?)
    var operationResult: (error: NSError?, sunriseTime: String?, sunsetTime: String?, totalDaylight: String?, daylightSavings: String?)
    
    override func main() {
        
        if self.cancelled {
            return
        }
        
        // make sure we have the all the data we need
        guard let sunriseTimeInMinutes = operationInput.sunriseTimeInMinutes,
            let sunsetTimeInMinutes = operationInput.sunsetTimeInMinutes else {
                
                operationResult.error = NSError(domain: Constants.errorDomain, code: Constants.requiredDataMissingErrorCode, userInfo: nil)
                return
        }
        
        // check to see if daylight savings time is in effect
        let isDST = NSTimeZone.systemTimeZone().isDaylightSavingTimeForDate(NSDate())
        
        // use the helper functions and local private functions to create the strings
        operationResult.sunriseTime = String.timeStringFromMinutes(sunriseTimeInMinutes, isDST: isDST)
        operationResult.sunsetTime = String.timeStringFromMinutes(sunsetTimeInMinutes, isDST: isDST)
        operationResult.totalDaylight = String.totalDaylightString(sunriseTimeInMinutes, sunsetTimeInMinutes: sunsetTimeInMinutes)
        operationResult.daylightSavings = daylightSavingsTimeString()
        
        // save some of the results to NSUserDefaults as well so that the Today Widget has access to them
        if let userDefaults = NSUserDefaults(suiteName: SuntrackerUserDefaultsConstants.UserDefaultsSuiteName) {
            userDefaults.setObject(operationResult.sunriseTime, forKey: SuntrackerUserDefaultsConstants.SunriseTimeUserDefaultsKey)
            userDefaults.setObject(operationResult.sunsetTime, forKey: SuntrackerUserDefaultsConstants.SunsetTimeUserDefaultsKey)
            userDefaults.setObject(operationResult.totalDaylight, forKey: SuntrackerUserDefaultsConstants.TotalDaylightUserDefaultsKey)
            userDefaults.setObject(operationResult.daylightSavings, forKey: SuntrackerUserDefaultsConstants.DaysUntilDSTSwitchUserDefaultsKey)
            
            // set the last calculated day to today
            let dayOfYear = NSDate.dayOfYear()
            userDefaults.setInteger(dayOfYear, forKey: SuntrackerUserDefaultsConstants.LastDayCalculatedUserDefaultsKey)
            
            // finally set the date that this operation last ran for debug info
            userDefaults.setObject(NSDate(), forKey: SuntrackerUserDefaultsConstants.LastStringsOperationUserDefaultsKey)
        }
    }
    
    private func daylightSavingsTimeString() -> String {
        
        // get the local timezone
        let localTimeZone = NSTimeZone.localTimeZone()
        
        // if nextDaylightSavingTimeTransition is nil then this timezone does not recognize DST
        guard let nextDaylightSavingTimeTransition = localTimeZone.nextDaylightSavingTimeTransition else {
            return "Daylight Savings Time is not observed"
        }
        
        // create a new NSCalendar and set its timezone
        let calendar = NSCalendar.currentCalendar()
        calendar.timeZone = localTimeZone
        
        // use the calendar to get the number of days until the next DST switch
        let componants = calendar.components([.Day], fromDate: NSDate(), toDate: nextDaylightSavingTimeTransition, options: NSCalendarOptions.MatchFirst)
        let dstPostFix = localTimeZone.daylightSavingTime ? "ends" : "starts"
        
        // create the string and return it
        let daylightSavingsTimeString = "\(componants.day) days until Daylight Savings Time \(dstPostFix)"
        return daylightSavingsTimeString
    }
}
