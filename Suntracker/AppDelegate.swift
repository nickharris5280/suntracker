//
//  AppDelegate.swift
//  Suntracker
//
//  Created by Nick Harris on 2/9/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    var sunTrackerViewController: SunTrackerViewController?
    var operationCoordinator: OperationCoordinator?
    var coreLocationManager: CLLocationManager?
    var lastLocation: CLLocation?

    // MARK: App Delegate
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // create the operation coordinator
        self.operationCoordinator = OperationCoordinator()
        
        // make sure we have the right rootViewController and dependency inject the operationCoordinator
        if let sunTrackerViewController = self.window?.rootViewController as? SunTrackerViewController {
            
            self.sunTrackerViewController = sunTrackerViewController
            sunTrackerViewController.operationCoordinator = operationCoordinator
        }
        
        // Setup the Location Manager
        setupCoreLocationManager()
        
        return true
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        
        // this is mainly to deal with whileInUse authorization. we need to get the latest location in that case.
        let status = CLLocationManager.authorizationStatus()
        handleLocationManagerStatus(status)
    }
    
    // MARK: Location Manager
    private func setupCoreLocationManager() {
        
        coreLocationManager = CLLocationManager()
        
        guard let coreLocationManager = coreLocationManager else {
            fatalError("coreLocationManager not set")
        }
        
        // set the properties on the location manager
        // pausesLocationUpdatesAutomatically may have no effect. I'm not sure. But there's no harm in saying the manager can.
        // activityType gives the manager more information about what the apps use is. Other is the best option for Suntracker
        coreLocationManager.pausesLocationUpdatesAutomatically = true
        coreLocationManager.activityType = .Other
        coreLocationManager.delegate = self
        
        // get the current authorization status and handle it appropriately
        let status = CLLocationManager.authorizationStatus()
        handleLocationManagerStatus(status)
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        print("locationManager:didChangeAuthorizationStatus")
        handleLocationManagerStatus(status)
    }
    
    func handleLocationManagerStatus(status: CLAuthorizationStatus) {
        
        switch status {
        case .NotDetermined:
            // need to ask for location permissions
            coreLocationManager?.requestAlwaysAuthorization()
            
        case .AuthorizedAlways:
            // register for notifications only when location permissions have been granted
            UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes:[ .Sound, .Alert], categories: nil))
            
            // the app doesn't need a precise location so no need for GPS which will impact battery use
            // instead use significant location changes
            coreLocationManager?.startMonitoringSignificantLocationChanges()
            
        case .AuthorizedWhenInUse:
            // register for notifications only when location permissions have been granted
            UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes:[ .Sound, .Alert], categories: nil))
            
            // if we're only allowed in-app locations theres no need to get location updates as its highly unlikely the user will
            // be moving great enough distances to alter sunrise/sunset times. Instead just request the current location
            coreLocationManager?.requestLocation()
            
        case .Denied:
            // we need to be sure the view is loaded so the alert can be presented
            if let sunTrackerViewController = sunTrackerViewController where sunTrackerViewController.isViewLoaded() {
                sunTrackerViewController.displayLocationPermissionsAlert()
            }
            
        case .Restricted:
            // we need to be sure the view is loaded so the alert can be presented
            if let sunTrackerViewController = sunTrackerViewController where sunTrackerViewController.isViewLoaded() {
                sunTrackerViewController.displayLocationPermissionsRestrictedAlert()
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // we got a new location - is it worth telling the app about?
        print("locationManager:didUpdateLocations")
    
        if locations.count > 0 {
            if let location = locations.last {
    
                // set lastLocation so we have one if the user is being prompted for notifications
                lastLocation = location
                
                // pass the location to the view controller
                sunTrackerViewController?.updateViewWithLocation(location)
                
                // if we have access to userDefaults, set the last location date and schedule notifications
                if let userDefaults = NSUserDefaults(suiteName: SuntrackerUserDefaultsConstants.UserDefaultsSuiteName) {
                    userDefaults.setObject(NSDate(), forKey: SuntrackerUserDefaultsConstants.LastLocationFetchUserDefaultsKey)
                    scheduleLocalNotifications(location, userDefaults: userDefaults)
                }
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        
        print("locationManager:didFailWithError: \(error.localizedDescription)")
        sunTrackerViewController?.displayLocationPermissionsAlert()
    }
    
    // MARK: Local Notifications
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        
        if let userDefaults = NSUserDefaults(suiteName: SuntrackerUserDefaultsConstants.UserDefaultsSuiteName) {
            if notificationSettings.types.contains(.Alert) {
                
                // turn on notifications for both sunrise and sunset in user defaults
                userDefaults.setBool(true, forKey: SuntrackerUserDefaultsConstants.SunriseNotificationsUserDefaultsKey)
                userDefaults.setBool(true, forKey: SuntrackerUserDefaultsConstants.SunsetNotificationsUserDefaultsKey)
                
                // if we have a location already we can now schedule the local notifications
                if let lastLocation = lastLocation {
                    scheduleLocalNotifications(lastLocation, userDefaults: userDefaults)
                }
            }
        }
    }
    
    private func scheduleLocalNotifications(location: CLLocation, userDefaults: NSUserDefaults) {
        
        // Cancel any notifications already scheduled
        let application = UIApplication.sharedApplication()
        application.cancelAllLocalNotifications()
        
        // if notifications are enabled (checked by looking for .Alert in notificationSettings)
        if let notificationSettings = application.currentUserNotificationSettings() where notificationSettings.types.contains(.Alert) {
            
            // get the users preferences from user defaults
            let sunriseLocalNotificationsEnabled = userDefaults.boolForKey(SuntrackerUserDefaultsConstants.SunriseNotificationsUserDefaultsKey)
            let sunsetLocalNotificationsEnabled = userDefaults.boolForKey(SuntrackerUserDefaultsConstants.SunsetNotificationsUserDefaultsKey)
            
            // if either sunrise or sunset notifications are enabled schedule new notifications
            if sunriseLocalNotificationsEnabled || sunsetLocalNotificationsEnabled {
                
                operationCoordinator?.createLocalNotifications(location.coordinate, sunriseLocalNotifications: sunriseLocalNotificationsEnabled, sunsetLocalNotifications: sunsetLocalNotificationsEnabled, application: application)
            }
        }
    }
}

