//
//  CreateLocalNotificationOperation.swift
//  Suntracker
//
//  Created by Nick Harris on 2/25/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import Foundation
import UIKit

class ScheduleLocalNotificationOperation: NSOperation {
    
    var operationInput: (date: NSDate?, timeInMinutes: Double?, localNotificationType: LocalNotificationType, application: UIApplication?)
    
    override init() {
        
        operationInput.date = nil
        operationInput.timeInMinutes = nil
        operationInput.localNotificationType = LocalNotificationType.Unknown
        operationInput.application = nil
        super.init()
    }
    
    override func main() {
        
        if self.cancelled {
            return
        }
        
        // Make sure we have the data required
        guard let date = operationInput.date,
              let timeInMinutes = operationInput.timeInMinutes,
              let application = operationInput.application where
              operationInput.localNotificationType != LocalNotificationType.Unknown else {
            
            fatalError("Not enough information to schedule a local notification")
        }
        
        // look to see if its DST and create a string containing the time from the minutes
        let isDST = NSTimeZone.systemTimeZone().isDaylightSavingTimeForDate(date)
        let timeString = String.timeStringFromMinutes(timeInMinutes, isDST: isDST)
        
        // create the date the local notificaiton should fire
        let fireDate = createFireDate(date, timeString: timeString)
        
        // make sure the date is in the future. scheduling a local notification with a date in the past will cause it to fire right away.
        if fireDate.isInFuture() {
            
            let locationNotification = createLocalNotification(fireDate, timeString: timeString, localNotificationType: operationInput.localNotificationType)
            application.scheduleLocalNotification(locationNotification)
        }
    }
    
    private func createFireDate(date: NSDate, timeString: String) -> NSDate {
        
        // create a date formatter for our time input string
        let inputDateFormatter = NSDateFormatter()
        inputDateFormatter.dateFormat = "yyyy-MM-dd"
        
        // create a string for the passed in date
        let dateString = inputDateFormatter.stringFromDate(date)
        
        // create a date and time formatter for the fire date
        let fireDateFormatter = NSDateFormatter()
        fireDateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        fireDateFormatter.timeZone = NSTimeZone.systemTimeZone()
        
        // combine the created date string and the passed in time string
        // and use the fireDateFormatter to create a new NSDate
        let fireDateString = dateString + " " + timeString
        let fireDate = fireDateFormatter.dateFromString(fireDateString)
        
        return fireDate!
    }
    
    private func createLocalNotification(fireDate: NSDate, timeString: String, localNotificationType: LocalNotificationType) -> UILocalNotification {
        
        let localNotification = UILocalNotification()
        localNotification.fireDate = fireDate
        localNotification.alertTitle = localNotificationType.alertTitle()
        localNotification.alertBody = localNotificationType.alertBody(timeString)
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.timeZone = NSTimeZone.systemTimeZone()
        
        return localNotification
    }
}
