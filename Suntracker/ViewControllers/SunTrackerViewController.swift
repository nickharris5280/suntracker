//
//  SunTrackerViewController.swift
//  Suntracker
//
//  Created by Nick Harris on 2/9/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit
import CoreLocation

class SunTrackerViewController: UIViewController, OperationCoordinatorViewController {
    
    @IBOutlet weak var daylabel: UILabel!
    @IBOutlet weak var localityLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var totalDaylightLabel: UILabel!
    @IBOutlet weak var daysUntilDSTSwitchLabel: UILabel!
    @IBOutlet weak var trendingChartView: UIView!
    @IBOutlet weak var trendingChartMidlineView: UIView!
    @IBOutlet weak var trendingChartSegmentedControl: UISegmentedControl!
    
    var trendingChartDataPoints: [TrendingChartData] = []
    
    var trendingChartValueLineView: UIView?
    
    var operationCoordinator: OperationCoordinator?
    var location: CLLocation?
    
    private var isShowingAlert: Bool = false
    
    // MARK: UIViewController Functions
    override func viewDidLoad() {
        
        super.viewDidLoad()
        trendingChartSegmentedControl?.addTarget(self, action: "calculateTrendingChart", forControlEvents: .ValueChanged);
        
        let selectedTrendChart = NSUserDefaults.standardUserDefaults().integerForKey(SuntrackerUserDefaultsConstants.SelectedTrendChartUserDefaultKey)
        trendingChartSegmentedControl?.selectedSegmentIndex = selectedTrendChart
        
        localityLabel.text = "----"
        sunriseLabel.text = "----"
        sunsetLabel.text = "----"
        totalDaylightLabel.text = "----"
        daysUntilDSTSwitchLabel.text = "----"
        
        trendingChartView.layer.borderWidth = 0.75
        trendingChartView.layer.borderColor = UIColor.darkGrayColor().CGColor
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "configureView", name: UIApplicationDidBecomeActiveNotification, object: nil)
        
        let dragOnTrendingViewGestureRecognizer = UIPanGestureRecognizer(target: self, action: "panTrendingChartGesture:")
        dragOnTrendingViewGestureRecognizer.maximumNumberOfTouches = 1
        dragOnTrendingViewGestureRecognizer.minimumNumberOfTouches = 1
        
        trendingChartView.addGestureRecognizer(dragOnTrendingViewGestureRecognizer)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        // its important the user know why the app isn't working anytime they load this view
        switch CLLocationManager.authorizationStatus() {
        case .Denied:
            displayLocationPermissionsAlert()
        case .Restricted:
            displayLocationPermissionsRestrictedAlert()
        default:
            break
        }
    }
    
    func configureView() {
        
        if let userDefaults = NSUserDefaults(suiteName: SuntrackerUserDefaultsConstants.UserDefaultsSuiteName),
           let location = location {
            
            // get the last day calculated and see if its the same as today
            let lastDayCalculated = userDefaults.integerForKey(SuntrackerUserDefaultsConstants.LastDayCalculatedUserDefaultsKey)
            let dayOfYear = NSDate.dayOfYear()
            
            // if its not then we need to recalculate the Today info
            if dayOfYear != lastDayCalculated {
                updateViewWithLocation(location)
            }
        }
        else {
            // make sure to update the lables from the Storyboard design values
            setLabelsFromUserDefaults()
        }
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        // The graph size changes when the device is rotate. This code makes sure it gets redrawn correctly.
        coordinator.animateAlongsideTransition({
                [unowned self]
                (context: UIViewControllerTransitionCoordinatorContext) in
            
                self.clearTrendingChart()
                self.totalDaylightLabel?.textAlignment = .Center
            },
            completion: {
                [unowned self]
                (context: UIViewControllerTransitionCoordinatorContext) in
                
                self.displayTrendingChart()
        })
    }
    
    override func willTransitionToTraitCollection(newCollection: UITraitCollection, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        // the totalDaylightLabel moves based on orientation. This makes sure it has the correct text alignment.
        if newCollection.verticalSizeClass == .Compact {
            totalDaylightLabel?.textAlignment = .Center
        }
        else {
            totalDaylightLabel?.textAlignment = .Right
        }
    }
    
    // MARK: Update View Functions
    func updateViewWithLocation(location: CLLocation) {
        
        // First look at the new location. If its not a great enough distance or hasn't been long enough there's no need to calculate an update to the UI.
        if let lastLocation = self.location {
            if lastLocation.distanceFromLocation(location) < SuntrackerNumericConstants.FifteenMilesInMeters &&
               Double(location.timestamp.timeIntervalSinceDate(lastLocation.timestamp)) < SuntrackerNumericConstants.SecondsInHour {
                return
            }
        }
        
        self.location = location
        setLocalityLabel()
        calculateTodayPanelLabels()
        calculateTrendingChart()
    }
    
    private func setLabelsFromUserDefaults() {
        
        if let userDefaults = NSUserDefaults(suiteName: SuntrackerUserDefaultsConstants.UserDefaultsSuiteName) {
            
            let lastLocality = userDefaults.objectForKey(SuntrackerUserDefaultsConstants.LastLocalityUserDefaultsKey) as? String
            let sunriseTime = userDefaults.objectForKey(SuntrackerUserDefaultsConstants.SunriseTimeUserDefaultsKey) as? String
            let sunsetTime = userDefaults.objectForKey(SuntrackerUserDefaultsConstants.SunsetTimeUserDefaultsKey) as? String
            let totalDaylight = userDefaults.objectForKey(SuntrackerUserDefaultsConstants.TotalDaylightUserDefaultsKey) as? String
            let daylightSavings = userDefaults.objectForKey(SuntrackerUserDefaultsConstants.DaysUntilDSTSwitchUserDefaultsKey) as? String
            
            daylabel.text = "Today"
            localityLabel.text = (lastLocality != .None) ? lastLocality : "----"
            sunriseLabel.text = (sunriseTime != .None) ? sunriseTime : "----"
            sunsetLabel.text = (sunsetTime != .None) ? sunsetTime : "----"
            totalDaylightLabel.text = (totalDaylight != .None) ? totalDaylight : "----"
            daysUntilDSTSwitchLabel.text = (daylightSavings != .None) ? daylightSavings : "----"
        }
    }
    
    private func setLocalityLabel() {
        
        if let location = location {
            
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(location) {
                
                [unowned self]
                (placemarks: [CLPlacemark]?, error: NSError?) in
                
                if let placemarks = placemarks where placemarks.count > 0,
                   let placemark = placemarks.first,
                   let locality = placemark.locality,
                   let administrativeArea = placemark.administrativeArea {

                    self.localityLabel?.text = locality + ", " + administrativeArea
                    
                    if let userDefaults = NSUserDefaults(suiteName: SuntrackerUserDefaultsConstants.UserDefaultsSuiteName) {
                        userDefaults.setObject(self.localityLabel?.text, forKey: SuntrackerUserDefaultsConstants.LastLocalityUserDefaultsKey)
                    }
                }
            }
        }
    }
    
    // Update Today Panel
    private func calculateTodayPanelLabels() {
        
        if let operationCoordinator = operationCoordinator,
           let location = location {
            
            operationCoordinator.calculateTodayInfoStrings(location.coordinate) {
                [unowned self]
                (error: NSError?, sunriseTime: String?, sunsetTime: String?, totalDaylight: String?, daylightSavings: String?) in
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    else if let sunriseTime = sunriseTime,
                            let sunsetTime = sunsetTime,
                            let totalDaylight = totalDaylight,
                            let daylightSavings = daylightSavings {
                        
                        self.sunriseLabel.text = sunriseTime
                        self.sunsetLabel.text = sunsetTime
                        self.totalDaylightLabel.text = totalDaylight
                        self.daysUntilDSTSwitchLabel.text = daylightSavings
                    }
                    else {
                        fatalError("No error reported but ViewController did not have enough information to set the bottom panel labels")
                    }
                })
            }
        }
    }
    
    // Update Trending Chart
    @objc private func calculateTrendingChart() {
        
        if let location = location {
            
            clearTrendingChart()
            
            NSUserDefaults.standardUserDefaults().setInteger(trendingChartSegmentedControl!.selectedSegmentIndex, forKey: SuntrackerUserDefaultsConstants.SelectedTrendChartUserDefaultKey)
            
            if let operationCoordinator = operationCoordinator,
               let trendingChartSegmentedControl = trendingChartSegmentedControl,
               let trendingChartType = TrendingChartType(rawValue: trendingChartSegmentedControl.selectedSegmentIndex) {
                
                operationCoordinator.createTrendingChart(trendingChartType, coordinate: location.coordinate) {
                    [unowned self]
                    (error: NSError?, totalSunlightHoursForChart: [TrendingChartData]) in
                    
                    dispatch_async(dispatch_get_main_queue(),{
                        [unowned self] in
                        
                        if let error = error {
                            print(error.localizedDescription)
                        }
                        else {
                            self.trendingChartDataPoints = totalSunlightHoursForChart
                            self.displayTrendingChart()
                        }
                    })
                }
            }
        }
    }
    
    private func clearTrendingChart() {
        if let trendingChartView = trendingChartView {
                
            for subview in trendingChartView.subviews {
                
                if subview != trendingChartMidlineView &&
                   subview != trendingChartValueLineView {
                    subview.removeFromSuperview()
                }
            }
        }
    }
    
    private func displayTrendingChart() {
        
        if let trendingChartView = trendingChartView,
           let trendingChartMidlineView = trendingChartMidlineView {
            
            // first figure out how wide each bar will be
            let barWidth = trendingChartView.frame.width / CGFloat(trendingChartDataPoints.count)
            let fullBarHeight = trendingChartView.frame.height
            
            // create a delay interval so the graph animates in like a wave
            let delayInterval = 0.33 / Double(trendingChartDataPoints.count)
            
            // setup the variables to draw the graph
            var x = CGFloat(0)
            var delay = 0.0
            
            // draw the graph
            for trendingChartData in trendingChartDataPoints {
                
                let barHeightPercentage = CGFloat(trendingChartData.totalMinutes / SuntrackerNumericConstants.TotalMinutesInDay)
                let calculatedBarHeight = fullBarHeight * barHeightPercentage
                let newViewFrame = CGRect(x: CGFloat(x), y: fullBarHeight, width: barWidth, height: CGFloat(0))
                
                let trendingChartBarView = TrendingChartBarView(frame: newViewFrame)
                trendingChartBarView.backgroundColor = UIColor(colorLiteralRed: 247.0/255.0, green: 222.0/255.0, blue: 15.0/255.0, alpha: 1.0)
                trendingChartBarView.trendingChartData = trendingChartData
                
                trendingChartView.addSubview(trendingChartBarView)
                
                UIView.animateWithDuration(0.33, delay: delay, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    
                    let animatedViewFrame = CGRect(x: CGFloat(x), y: CGFloat(fullBarHeight - calculatedBarHeight), width: barWidth, height: CGFloat(calculatedBarHeight))
                    trendingChartBarView.frame = animatedViewFrame
                    
                    },
                    completion: nil)
                
                delay = delay + delayInterval
                x = x + barWidth
            }
            
            // make sure the midline is visible over the newly added bar graph views
            trendingChartView.bringSubviewToFront(trendingChartMidlineView)
        }
    }
    
    // MARK: Trending Chart Touch and Pan
    
    // Need to override touchesBegan so we can start displaying chart data right away without the user needing to start dragging across the chart
    // Tap gestures seem to only register after the finger is lifted. This approach works great and is fairly simple.
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            if let _ = touch.view as? TrendingChartBarView {
                
                showTrendingChartValue(touch.locationInView(trendingChartView))
            }
        }
    }
    
    // Hide all the chart data from the today panel when touches end
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        trendingChartValueLineView?.removeFromSuperview()
        setLabelsFromUserDefaults()
    }
    
    // Get the point that the users finger is at while dragging across the chart and display the appropriate information
    func panTrendingChartGesture(gestureRecognizer: UIPanGestureRecognizer) {
        
        let locationInTrendingChartView = gestureRecognizer.locationInView(trendingChartView)
        
        showTrendingChartValue(locationInTrendingChartView)
        
        if (gestureRecognizer.state == .Ended) {
            
            trendingChartValueLineView?.removeFromSuperview()
            setLabelsFromUserDefaults()
        }
    }
    
    // MARK: Display Trending Chart Value
    
    // Redraw the line showing what information is being shown as well as pulling the graph data from the TrendingChartBarView
    // that is under the users finger.
    func showTrendingChartValue(locationInTrendingChartView: CGPoint) {
        
        trendingChartValueLineView?.removeFromSuperview()
        
        for subview in (trendingChartView?.subviews)! {
            
            if let trendingChartBarView = subview as? TrendingChartBarView {
                if (CGRectContainsPoint(subview.frame, locationInTrendingChartView)) {
                    
                    daylabel.text = trendingChartBarView.trendingChartData?.dateString
                    sunriseLabel.text = trendingChartBarView.trendingChartData?.sunriseTime
                    sunsetLabel.text = trendingChartBarView.trendingChartData?.sunsetTime
                    totalDaylightLabel.text = trendingChartBarView.trendingChartData?.totalDaylight
                    
                    trendingChartValueLineView?.removeFromSuperview()
                    trendingChartValueLineView = UIView(frame: CGRectMake(locationInTrendingChartView.x, 0, 1, (trendingChartView?.frame.height)!))
                    trendingChartValueLineView!.backgroundColor = UIColor.blackColor()
                    
                    trendingChartView.addSubview(trendingChartValueLineView!)
                    
                    break
                }
            }
        }
    }
    
    // MARK: Location Error Alert
    func displayLocationPermissionsAlert() {
        
        if isShowingAlert {
            return
        }
        
        let alertController = UIAlertController(title: "Could Not Determine Location", message: "Suntracker determines the sunrise and sunset times by using your location. Please be sure you have location permissions enabled in the Settings App.", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) {
            [unowned self]
            action in
            
            self.isShowingAlert = false
        }
        alertController.addAction(cancelAction)
        
        let settingsAction = UIAlertAction(title: "Open Settings App", style: .Default) {
            [unowned self]
            action in
            
            self.isShowingAlert = false
            if let url = NSURL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alertController.addAction(settingsAction)
        
        self.isShowingAlert = true
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func displayLocationPermissionsRestrictedAlert() {
        
        if isShowingAlert {
            return
        }
        
        let alertController = UIAlertController(title: "Location Use Restricted", message: "Suntracker is not permitted to use your location due to device restrictions.", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .Cancel) {
            [unowned self]
            action in
            
            self.isShowingAlert = false
        }
        alertController.addAction(cancelAction)
        
        self.isShowingAlert = true
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: Segues
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if  let navigationController = segue.destinationViewController as? UINavigationController,
            let suntrackerSettingsTableViewController = navigationController.topViewController as? SuntrackerSettingsTableViewController {
            
            suntrackerSettingsTableViewController.operationCoordinator = operationCoordinator
            suntrackerSettingsTableViewController.location = location
        }
    }
    
    // Needed for the Done button on the settings page
    @IBAction func unwindToSunTrackerViewController(segue: UIStoryboardSegue) {
    }
}
