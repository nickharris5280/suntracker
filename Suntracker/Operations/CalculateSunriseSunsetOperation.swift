//
//  CalculateSunriseSunsetOperation.swift
//  Suntracker
//
//  Created by Nick Harris on 2/10/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import Foundation
import CloudKit

class CalculateSunriseSunsetOperation: NSOperation {
    
    // MARK: Public Properties
    var operationInput: (coordinate: CLLocationCoordinate2D?, date: NSDate?)
    var operationResult: (error: NSError?, sunriseTimeInMinutes: Double?, sunsetTimeInMinutes: Double?)
    
    override init() {
        
        operationInput = (coordinate: nil, date: nil)
        
        super.init()
    }
    
    override func main() {
        
        if self.cancelled {
            return
        }
        
        // Make sure we have the data we need to proceed
        guard let coordinate = operationInput.coordinate,
            let date = operationInput.date else {
                fatalError("Not enough information to execute CalculateSunriseSunsetOperation")
        }
        
        // get the Julian Day for the date passed in
        let JD = calculateJulianDay(date)
        
        // calculate the sunrise and sunset times
        let sunriseTimeUTCMinutes = calculateSunriseUTC(JD, latitude: coordinate.latitude, longitude: coordinate.longitude)
        let sunsetTimeUTCMinutes = calculateSunsetUTC(JD, latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        // figure out how many minutes from GMT
        let minutesFromGMT = Double(NSTimeZone.localTimeZone().secondsFromGMT / 60)
        
        // subtract the minutes from GTM to get the minutes represented in local time
        operationResult.sunriseTimeInMinutes = sunriseTimeUTCMinutes - minutesFromGMT
        operationResult.sunsetTimeInMinutes = sunsetTimeUTCMinutes - minutesFromGMT
    }
    
    //***************************************************************************
    //
    // function calcSunriseUTC(JD, latitude, longitude)
    // {
    //     var t = calcTimeJulianCent(JD);
    //
    //     *** Find the time of solar noon at the location, and use
    //         that declination. This is better than start of the
    //         Julian day
    //
    //     var noonmin = calcSolNoonUTC(t, longitude);
    //     var tnoon = calcTimeJulianCent (JD+noonmin/1440.0);
    //
    //     *** First pass to approximate sunrise (using solar noon)
    //
    //     var eqTime = calcEquationOfTime(tnoon);
    //     var solarDec = calcSunDeclination(tnoon);
    //     var hourAngle = calcHourAngleSunrise(latitude, solarDec);
    //
    //     var delta = longitude - radToDeg(hourAngle);
    //     var timeDiff = 4 * delta;	// in minutes of time
    //     var timeUTC = 720 + timeDiff - eqTime;	// in minutes
    //
    //     *** Second pass includes fractional jday in gamma calc
    //
    //     var newt = calcTimeJulianCent(calcJDFromJulianCent(t) + timeUTC/1440.0);
    //     eqTime = calcEquationOfTime(newt);
    //     solarDec = calcSunDeclination(newt);
    //     hourAngle = calcHourAngleSunrise(latitude, solarDec);
    //     delta = longitude - radToDeg(hourAngle);
    //     timeDiff = 4 * delta;
    //     timeUTC = 720 + timeDiff - eqTime; // in minutes
    //
    //     return timeUTC;
    // }
    //
    //***************************************************************************
    
    private func calculateSunriseUTC(JD: Double, latitude: Double, longitude: Double) -> Double {
        
        let t = calculateTimeJulianCentury(JD)
        let noonmin = calculateSolarNoonUTC(t, longitude: longitude);
        let tnoon = calculateTimeJulianCentury(JD + noonmin / 1440.0)
        
        //     *** First pass to approximate sunrise (using solar noon)
        
        var eqTime = calculateEquationOfTime(tnoon)
        var solarDeclination = calculateSunDeclination(tnoon)
        var hourAngle = calculateHourAngleSunrise(latitude, solarDeclination: solarDeclination)
        
        var delta = longitude - radiansToDegrees(hourAngle)
        var timeDiff = 4 * delta
        var timeUTC = 720 + timeDiff - eqTime
        
        // *** Second pass includes fractional jday in gamma calc
        
        let newt = calculateTimeJulianCentury(calculateJulianDayFromJulianCentury(t) + timeUTC/1440.0);
        
        eqTime = calculateEquationOfTime(newt)
        solarDeclination = calculateSunDeclination(newt)
        hourAngle = calculateHourAngleSunrise(latitude, solarDeclination: solarDeclination)
        
        delta = longitude - radiansToDegrees(hourAngle)
        timeDiff = 4 * delta
        timeUTC = 720 + timeDiff - eqTime
        
        return timeUTC
    }
    
    //***************************************************************************
    //
    // function calcSunsetUTC(JD, latitude, longitude)
    // {
    //   var t = calcTimeJulianCent(JD);
    //
    //   // *** Find the time of solar noon at the location, and use
    //   //     that declination. This is better than start of the
    //   //     Julian day
    //
    //   var noonmin = calcSolNoonUTC(t, longitude);
    //   var tnoon = calcTimeJulianCent (JD+noonmin/1440.0);
    //
    //   // First calculates sunrise and approx length of day
    //
    //   var eqTime = calcEquationOfTime(tnoon);
    //   var solarDec = calcSunDeclination(tnoon);
    //   var hourAngle = calcHourAngleSunset(latitude, solarDec);
    //
    //   var delta = longitude - radToDeg(hourAngle);
    //   var timeDiff = 4 * delta;
    //   var timeUTC = 720 + timeDiff - eqTime;
    //
    //   // first pass used to include fractional day in gamma calc
    //
    //   var newt = calcTimeJulianCent(calcJDFromJulianCent(t) + timeUTC/1440.0);
    //   eqTime = calcEquationOfTime(newt);
    //   solarDec = calcSunDeclination(newt);
    //   hourAngle = calcHourAngleSunset(latitude, solarDec);
    //
    //   delta = longitude - radToDeg(hourAngle);
    //   timeDiff = 4 * delta;
    //   timeUTC = 720 + timeDiff - eqTime; // in minutes
    //
    //   return timeUTC;
    // }
    //
    //***************************************************************************
    
    private func calculateSunsetUTC(JD: Double, latitude: Double, longitude: Double) -> Double {
        
        let t = calculateTimeJulianCentury(JD)
        let noonmin = calculateSolarNoonUTC(t, longitude: longitude);
        let tnoon = calculateTimeJulianCentury(JD + noonmin / 1440.0)
        
        //     *** First pass to approximate sunrise (using solar noon)
        
        var eqTime = calculateEquationOfTime(tnoon)
        var solarDeclination = calculateSunDeclination(tnoon)
        var hourAngle = calculateHourAngleSunset(latitude, solarDeclination: solarDeclination)
        
        var delta = longitude - radiansToDegrees(hourAngle)
        var timeDiff = 4 * delta
        var timeUTC = 720 + timeDiff - eqTime
        
        // *** Second pass includes fractional jday in gamma calc
        
        let newt = calculateTimeJulianCentury(calculateJulianDayFromJulianCentury(t) + timeUTC/1440.0);
        
        eqTime = calculateEquationOfTime(newt)
        solarDeclination = calculateSunDeclination(newt)
        hourAngle = calculateHourAngleSunset(latitude, solarDeclination: solarDeclination)
        
        delta = longitude - radiansToDegrees(hourAngle)
        timeDiff = 4 * delta
        timeUTC = 720 + timeDiff - eqTime
        
        return timeUTC
    }
    
    //***************************************************************************
    //
    // function calcHourAngleSunrise(lat, solarDec)
    // {
    //   var latRad = degToRad(lat);
    //   var sdRad  = degToRad(solarDec)
    //
    //   var HAarg = (Math.cos(degToRad(90.833))/(Math.cos(latRad)*Math.cos(sdRad))-Math.tan(latRad) * Math.tan(sdRad));
    //
    //   var HA = (Math.acos(Math.cos(degToRad(90.833))/(Math.cos(latRad)*Math.cos(sdRad))-Math.tan(latRad) * Math.tan(sdRad)));
    //
    //   return HA;		// in radians
    // }
    //
    //***************************************************************************
    
    private func calculateHourAngleSunrise(latitude: Double, solarDeclination: Double) -> Double {
        
        let latitudeRadians = degreesToRadians(latitude)
        let solarDeclinationRadians = degreesToRadians(solarDeclination)
        
        let HA = (acos(cos(degreesToRadians(90.833))/(cos(latitudeRadians)*cos(solarDeclinationRadians))-tan(latitudeRadians) * tan(solarDeclinationRadians)))
        
        return HA
    }
    
    //***************************************************************************
    //
    // function calcHourAngleSunset(lat, solarDec)
    // {
    //   var latRad = degToRad(lat);
    //   var sdRad  = degToRad(solarDec)
    //
    //   var HAarg = (Math.cos(degToRad(90.833))/(Math.cos(latRad)*Math.cos(sdRad))-Math.tan(latRad) * Math.tan(sdRad));
    //
    //   var HA = (Math.acos(Math.cos(degToRad(90.833))/(Math.cos(latRad)*Math.cos(sdRad))-Math.tan(latRad) * Math.tan(sdRad)));
    //
    //   return -HA;		// in radians
    // }
    //
    //***************************************************************************
    
    private func calculateHourAngleSunset(latitude: Double, solarDeclination: Double) -> Double {
        
        let latitudeRadians = degreesToRadians(latitude)
        let solarDeclinationRadians = degreesToRadians(solarDeclination)
        
        let HA = (acos(cos(degreesToRadians(90.833))/(cos(latitudeRadians)*cos(solarDeclinationRadians))-tan(latitudeRadians) * tan(solarDeclinationRadians)))
        
        return HA * -1
    }
    
    //***************************************************************************
    //
    // function calcSunDeclination(t)
    // {
    //   var e = calcObliquityCorrection(t);
    //   var lambda = calcSunApparentLong(t);
    //
    //   var sint = Math.sin(degToRad(e)) * Math.sin(degToRad(lambda));
    //   var theta = radToDeg(Math.asin(sint));
    //   return theta;		// in degrees
    // }
    //
    //***************************************************************************
    
    private func calculateSunDeclination(t: Double) -> Double {
        
        let e = calculateObliquityCorrection(t)
        let lambda = calculateSunApparentLong(t)
        
        let sint = sin(degreesToRadians(e)) * sin(degreesToRadians(lambda))
        let theta = radiansToDegrees(asin(sint))
        
        return theta
    }
    
    //***************************************************************************
    //
    // function calcSunApparentLong(t)
    // {
    //   var o = calcSunTrueLong(t);
    //
    //   var omega = 125.04 - 1934.136 * t;
    //   var lambda = o - 0.00569 - 0.00478 * Math.sin(degToRad(omega));
    //   return lambda;		// in degrees
    // }
    //
    //***************************************************************************
    
    private func calculateSunApparentLong(t: Double) -> Double {
        
        let o = calculateSunTrueLong(t)
        
        let omega = 125.04 - 1934.136 * t
        let lambda = o - 0.00569 - 0.00478 * sin(degreesToRadians(omega))
        
        return lambda
    }
    
    //***************************************************************************
    //
    // function calcSunTrueLong(t)
    // {
    //   var l0 = calcGeomMeanLongSun(t);
    //   var c = calcSunEqOfCenter(t);
    //
    //   var O = l0 + c;
    //   return O;		// in degrees
    // }
    //
    //***************************************************************************
    
    private func calculateSunTrueLong(t: Double) -> Double {
        
        let l0 = calculateGeoMeanLongSun(t)
        let c = calculateSunEqOfCenter(t)
        
        let O = l0 + c
        
        return O
    }
    
    //***************************************************************************
    //
    // function calcSunEqOfCenter(t)
    // {
    //   var m = calcGeomMeanAnomalySun(t);
    //
    //   var mrad = degToRad(m);
    //   var sinm = Math.sin(mrad);
    //   var sin2m = Math.sin(mrad+mrad);
    //   var sin3m = Math.sin(mrad+mrad+mrad);
    //
    //   var C = sinm * (1.914602 - t * (0.004817 + 0.000014 * t)) + sin2m * (0.019993 - 0.000101 * t) + sin3m * 0.000289;
    //   return C;		// in degrees
    // }
    //
    //***************************************************************************
    
    private func calculateSunEqOfCenter(t: Double) -> Double {
        
        let m = calculateGeomMeanAnomalySun(t)
        
        let mrad = degreesToRadians(m)
        let sinm = sin(mrad)
        let sin2m = sin(mrad + mrad)
        let sin3m = sin(mrad + mrad + mrad)
        
        let C = sinm * (1.914602 - t * (0.004817 + 0.000014 * t)) + sin2m * (0.019993 - 0.000101 * t) + sin3m * 0.000289
        
        return C
    }
    
    //***************************************************************************
    //
    // function calcSolNoonUTC(t, longitude)
    // {
    //   // First pass uses approximate solar noon to calculate eqtime
    //   var tnoon = calcTimeJulianCent(calcJDFromJulianCent(t) + longitude/360.0);
    //   var eqTime = calcEquationOfTime(tnoon);
    //   var solNoonUTC = 720 + (longitude * 4) - eqTime; // min
    //
    //   var newt = calcTimeJulianCent(calcJDFromJulianCent(t) -0.5 + solNoonUTC/1440.0);
    //
    //   eqTime = calcEquationOfTime(newt);
    //   // var solarNoonDec = calcSunDeclination(newt);
    //   solNoonUTC = 720 + (longitude * 4) - eqTime; // min
    //
    //   return solNoonUTC;
    // }
    //
    //***************************************************************************
    
    private func calculateSolarNoonUTC(t: Double, longitude: Double) -> Double {
        
        let tnoon = calculateTimeJulianCentury(calculateJulianDayFromJulianCentury(t) + longitude / 360.0)
        var eqTime = calculateEquationOfTime(tnoon)
        var solarNoonUTC = 720 + (longitude * 4) - eqTime
        
        let newt = calculateTimeJulianCentury(calculateJulianDayFromJulianCentury(t) - 0.5 + solarNoonUTC / 1440.0 )
        
        eqTime = calculateEquationOfTime(newt)
        solarNoonUTC = 720 + (longitude * 4) - eqTime
        
        return solarNoonUTC
    }
    
    //***************************************************************************
    //
    // function calcEquationOfTime(t)
    // {
    //   var epsilon = calcObliquityCorrection(t);
    //   var l0 = calcGeomMeanLongSun(t);
    //   var e = calcEccentricityEarthOrbit(t);
    //   var m = calcGeomMeanAnomalySun(t);
    //
    //   var y = Math.tan(degToRad(epsilon)/2.0);
    //   y *= y;
    //
    //   var sin2l0 = Math.sin(2.0 * degToRad(l0));
    //   var sinm   = Math.sin(degToRad(m));
    //   var cos2l0 = Math.cos(2.0 * degToRad(l0));
    //   var sin4l0 = Math.sin(4.0 * degToRad(l0));
    //   var sin2m  = Math.sin(2.0 * degToRad(m));
    //
    //   var Etime = y * sin2l0 - 2.0 * e * sinm + 4.0 * e * y * sinm * cos2l0
	//   			- 0.5 * y * y * sin4l0 - 1.25 * e * e * sin2m;
    //
    //   return radToDeg(Etime)*4.0;	// in minutes of time
    // }
    //
    //***************************************************************************
    
    private func calculateEquationOfTime(t: Double) -> Double {
        
        let epsilon = calculateObliquityCorrection(t)
        let l0 = calculateGeoMeanLongSun(t)
        let e = calculateEccentricityEarthOrbit(t)
        let m = calculateGeomMeanAnomalySun(t)
        
        var y = tan(degreesToRadians(epsilon) / 2.0)
        y = y*y
        
        let sin2l0 = sin(2.0 * degreesToRadians(l0))
        let sinm = sin(degreesToRadians(m))
        let cos2l0 = cos(2.0 * degreesToRadians(l0))
        let sin4l0 = sin(4.0 * degreesToRadians(l0))
        let sin2m = sin(2.0 * degreesToRadians(m))
        
        var Etime = y * sin2l0 - 2.0 * e * sinm + 4.0 * e * y * sinm * cos2l0 - 0.5 * y * y * sin4l0 - 1.25 * e * e * sin2m
        Etime = radiansToDegrees(Etime) * 4.0
        
        return Etime
    }
    
    //***************************************************************************
    //
    // function calcGeomMeanAnomalySun(t)
    // {
    //   var M = 357.52911 + t * (35999.05029 - 0.0001537 * t);
    //   return M;		// in degrees
    // }
    //
    //***************************************************************************
    
    private func calculateGeomMeanAnomalySun(t: Double) -> Double {
        
        let M = 357.52911 + t * (35999.05029 - 0.0001537 * t)
        return M
    }
    
    //***************************************************************************
    //
    // function calcEccentricityEarthOrbit(t)
    // {
    //   var e = 0.016708634 - t * (0.000042037 + 0.0000001267 * t);
    //   return e;		// unitless
    // }
    //
    //***************************************************************************
    
    private func calculateEccentricityEarthOrbit(t: Double) -> Double {
        
        let e = 0.016708634 - t * (0.000042037 + 0.0000001267 * t)
        return e
    }
    
    //***************************************************************************
    //
    // function calcGeomMeanLongSun(t)
    // {
    //   var L0 = 280.46646 + t * (36000.76983 + 0.0003032 * t);
    //   while(L0 > 360.0)
    //   {
    //     L0 -= 360.0;
    //   }
    //   while(L0 < 0.0)
    //   {
    //     L0 += 360.0;
    //   }
    //   return L0;		// in degrees
    // }
    //
    //***************************************************************************
    
    private func calculateGeoMeanLongSun(t: Double) -> Double {
        
        var L0 = 280.46646 + t * (36000.76983 + 0.0003032 * t)
        
        while (L0 > 360.0) {
            L0 = L0 - 360.0
        }
        while (L0 < 0.0) {
            L0 = L0 + 360.0
        }
        
        return L0
    }
    
    //***************************************************************************
    //
    // function calcObliquityCorrection(t)
    // {
    //   var e0 = calcMeanObliquityOfEcliptic(t);
    //
    //   var omega = 125.04 - 1934.136 * t;
    //   var e = e0 + 0.00256 * Math.cos(degToRad(omega));
    //   return e;		// in degrees
    // }
    //
    //***************************************************************************
    
    private func calculateObliquityCorrection(t: Double) -> Double {
        
        let e0 = calculateMeanObliquityOfEcliptic(t)
        
        let omega = 125.04 - 1934.136 * t
        let e = e0 + 0.00256 * cos(degreesToRadians(omega))
        
        return e
    }
    
    //***************************************************************************
    //
    // function calcMeanObliquityOfEcliptic(t)
    // {
    //   var seconds = 21.448 - t*(46.8150 + t*(0.00059 - t*(0.001813)));
    //   var e0 = 23.0 + (26.0 + (seconds/60.0))/60.0;
    //   return e0;		// in degrees
    // }
    //
    //***************************************************************************
    
    private func calculateMeanObliquityOfEcliptic(t: Double) -> Double {
        
        let seconds = 21.448 - t * (46.8150 + t * (0.00059 - t * (0.001813)))
        let e0 = 23.0 + (26.0 + (seconds / 60.0)) / 60.0
        
        return e0
    }
    
    //***************************************************************************
    //
    // function calcTimeJulianCent(jd)
    // {
    //   var T = (jd - 2451545.0)/36525.0;
    //   return T;
    // }
    //
    //***************************************************************************
    
    private func calculateTimeJulianCentury(jd: Double) -> Double {

        let T = (jd - 2451545.0) / 36525.0
        return T
    }
    
    //***************************************************************************
    //
    // function calcJDFromJulianCent(t)
    // {
    //   var JD = t * 36525.0 + 2451545.0;
    //   return JD;
    // }
    //
    //***************************************************************************
    
    private func calculateJulianDayFromJulianCentury(t: Double) -> Double {
        
        let JD = t * 36525.0 + 2451545.0
        return JD
    }
    
    //***************************************************************************
    //
    // function calcJD(year, month, day)
    // {
    //   if (month <= 2) {
    //     year -= 1;
    //     month += 12;
    //   }
    //
    //   var A = Math.floor(year/100);
    //   var B = 2 - A + Math.floor(A/4);
    //
    //   var JD = Math.floor(365.25*(year + 4716)) + Math.floor(30.6001*(month+1)) + day + B - 1524.5;
    //   return JD;
    // }
    //
    //***************************************************************************
    
    private func calculateJulianDay(date: NSDate) -> Double {
        
        let components = NSCalendar.currentCalendar().components([NSCalendarUnit.Month, NSCalendarUnit.Day, NSCalendarUnit.Year], fromDate: date)
        let day = Double(components.day)
        var month = Double(components.month)
        var year = Double(components.year)
        
        if (month <= 2) {
            year = year - 1
            month = month + 12
        }
        
        let A = floor(Double(year) / 100.0)
        let B = 2 - A + floor(A / 4)
        let JD = floor(365.25 * (year + 4716)) + floor(30.6001 * (month + 1)) + day + B - 1524.5
        
        return JD
    }
    
    private func radiansToDegrees(radians: Double) -> Double
    {
        return (180.0 * radians / M_PI);
    }
    
    private func degreesToRadians(degrees: Double) -> Double
    {
        return (M_PI * degrees / 180.0)
    }
}
