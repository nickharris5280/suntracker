//
//  TodayViewController.swift
//  Suntracker Widget
//
//  Created by Nick Harris on 2/19/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit
import NotificationCenter
import CoreLocation

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunriseValueLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var sunsetValueLabel: UILabel!
    @IBOutlet weak var totalDaylightLabel: UILabel!
    @IBOutlet weak var totalDaylightValueLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    
    private var sunriseTime: String?
    private var sunsetTime: String?
    private var totalDaylight: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredContentSize = CGSizeMake(0, 95);

        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "openApp"))
    }
    
    override func viewWillAppear(animated: Bool) {

        super.viewWillAppear(animated)

        errorLabel.hidden = true
        updateLabels(nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetMarginInsetsForProposedMarginInsets
        (defaultMarginInsets: UIEdgeInsets) -> (UIEdgeInsets) {
            let newEdgeInsets = UIEdgeInsets(top: defaultMarginInsets.top, left: defaultMarginInsets.left, bottom: 8, right: 12)
            return newEdgeInsets
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        
        updateLabels(completionHandler)
    }
    
    private func updateLabels(completionHandler: ((NCUpdateResult) -> Void)?) {
        
        // Get the userDefaults for the suite as well as the lastDayCalculated.
        // If the userDefaults doesn't exist we can assume that we don't have location permissions
        if let userDefaults = NSUserDefaults(suiteName: SuntrackerUserDefaultsConstants.UserDefaultsSuiteName) {
            
            // get the last day calculated and see if its the same as today
            let lastDayCalculated = userDefaults.integerForKey(SuntrackerUserDefaultsConstants.LastDayCalculatedUserDefaultsKey)
            let dayOfYear = NSDate.dayOfYear()
            if dayOfYear == lastDayCalculated {
                
                // it is - assume all of the values are set correctly and just read them back out
                sunriseTime = userDefaults.objectForKey(SuntrackerUserDefaultsConstants.SunriseTimeUserDefaultsKey) as? String
                sunsetTime = userDefaults.objectForKey(SuntrackerUserDefaultsConstants.SunsetTimeUserDefaultsKey) as? String
                totalDaylight = userDefaults.objectForKey(SuntrackerUserDefaultsConstants.TotalDaylightUserDefaultsKey) as? String
                
                setLabelsWithCompletionHandler(completionHandler)
            }
            else if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse ||
                    CLLocationManager.authorizationStatus() == .AuthorizedAlways {
        
                // if we have permission for location, check to see if we have one we can use
                // using locationDidUpdate in an extension does not work
                
                let coreLocationManager = CLLocationManager()
                if let location  = coreLocationManager.location {
                    
                    // we have a location - we can make the calculations
                    let operationController = OperationCoordinator()
                    operationController.calculateTodayInfoStrings(location.coordinate) {
                        [unowned self]
                        (error: NSError?, sunriseTime: String?, sunsetTime: String?, totalDaylight: String?, daylightSavings: String?) in
                        
                        dispatch_async(dispatch_get_main_queue(),{
                            
                            self.sunriseTime = sunriseTime
                            self.sunsetTime = sunsetTime
                            self.totalDaylight = totalDaylight
                            
                            self.setLabelsWithCompletionHandler(completionHandler)
                        })
                    }
                }
                else {
                    showErrorLabel(completionHandler)
                }
            }
            else {
                showErrorLabel(completionHandler)
            }
        }
    }
    
    private func setLabelsWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)?) {
        
        errorLabel.hidden = true
        sunriseLabel.hidden = false
        sunriseValueLabel.hidden = false
        sunsetLabel.hidden = false
        sunsetValueLabel.hidden = false
        totalDaylightLabel.hidden = false
        totalDaylightValueLabel.hidden = false
        
        sunriseValueLabel.text = (sunriseTime != .None) ? sunriseTime : "----"
        sunsetValueLabel.text = (sunsetTime != .None) ? sunsetTime : "----"
        totalDaylightValueLabel.text = (totalDaylight != .None) ? totalDaylight : "----"
                
        if let completionHandler = completionHandler {
            completionHandler(.NewData)
        }
    }
    
    private func showErrorLabel(completionHandler: ((NCUpdateResult) -> Void)?) {
        
        errorLabel.hidden = false
        sunriseLabel.hidden = true
        sunriseValueLabel.hidden = true
        sunsetLabel.hidden = true
        sunsetValueLabel.hidden = true
        totalDaylightLabel.hidden = true
        totalDaylightValueLabel.hidden = true
        
        if let completionHandler = completionHandler {
            completionHandler(.NewData)
        }
    }
    
    func openApp() {
        extensionContext?.openURL(NSURL(string: "suntracker://")!, completionHandler: nil)
    }
}
