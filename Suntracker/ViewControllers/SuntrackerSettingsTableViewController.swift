//
//  SuntrackerSettingsTableViewController.swift
//  Suntracker
//
//  Created by Nick Harris on 2/24/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit
import CoreLocation

class SuntrackerSettingsTableViewController: UITableViewController, OperationCoordinatorViewController {
    
    @IBOutlet weak var locationAccessNeverCell: UITableViewCell!
    @IBOutlet weak var locationAccessWhileUsingAppCell: UITableViewCell!
    @IBOutlet weak var locationAccessAlwaysCell: UITableViewCell!
    @IBOutlet weak var allowNotificationsSwitch: UISwitch!
    @IBOutlet weak var sunriseNotificationsSwitch: UISwitch!
    @IBOutlet weak var sunsetNotificationSwitch: UISwitch!
    @IBOutlet weak var lastLocationUpdateLabel: UILabel!
    @IBOutlet weak var lastTodayCalculationLabel: UILabel!
    
    var operationCoordinator: OperationCoordinator?
    var location: CLLocation?
    
    // MARK: UIViewController Functions
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        configureView()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "configureView", name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    @objc private func configureView() {
        configureNotificationSwitches()
        configureLastUpdateLabels()
    }
    
    // MARK: Notification Settings
    private func configureNotificationSwitches() {
        
        let application = UIApplication.sharedApplication()
        if let settings = application.currentUserNotificationSettings()
        {
            if settings.types.contains([.Alert])
            {
                allowNotificationsSwitch.on = true
                sunriseNotificationsSwitch.enabled = true
                sunsetNotificationSwitch.enabled = true
                
                if application.scheduledLocalNotifications?.count == 0 {
                    scheduleLocalNotifications()
                }
            }
            else
            {
                allowNotificationsSwitch.on = false
                sunriseNotificationsSwitch.on = false
                sunsetNotificationSwitch.on = false
                
                sunriseNotificationsSwitch.enabled = false
                sunsetNotificationSwitch.enabled = false
            }
        }
        
        if let userDefaults = NSUserDefaults(suiteName: SuntrackerUserDefaultsConstants.UserDefaultsSuiteName) {
            
            sunriseNotificationsSwitch.on = userDefaults.boolForKey(SuntrackerUserDefaultsConstants.SunriseNotificationsUserDefaultsKey)
            sunsetNotificationSwitch.on = userDefaults.boolForKey(SuntrackerUserDefaultsConstants.SunsetNotificationsUserDefaultsKey)
        }
        else {
            sunriseNotificationsSwitch.on = false
            sunsetNotificationSwitch.on = false
            sunriseNotificationsSwitch.enabled = false
            sunsetNotificationSwitch.enabled = false
        }
    }
    
    // MARK: Last update debug info
    private func configureLastUpdateLabels() {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        if let userDefaults = NSUserDefaults(suiteName: SuntrackerUserDefaultsConstants.UserDefaultsSuiteName) {
            if let lastLocationUpdate = userDefaults.objectForKey(SuntrackerUserDefaultsConstants.LastLocationFetchUserDefaultsKey) as? NSDate {
                lastLocationUpdateLabel.text = dateFormatter.stringFromDate(lastLocationUpdate)
            }
            if let lastTodayCalculation = userDefaults.objectForKey(SuntrackerUserDefaultsConstants.LastStringsOperationUserDefaultsKey) as? NSDate {
                lastTodayCalculationLabel.text = dateFormatter.stringFromDate(lastTodayCalculation)
            }
        }
    }
    
    // MARK: IBActions
    @IBAction func allowNotificationsSwitchDidChangeValue() {
        allowNotificationsSwitch.on = !allowNotificationsSwitch.on
        displayPermissionsAlert()
    }
    
    @IBAction func changeNotificationsSetting(sender: UISwitch) {
        
        if let userDefaults = NSUserDefaults(suiteName: SuntrackerUserDefaultsConstants.UserDefaultsSuiteName) {
        
            if sender == sunriseNotificationsSwitch {
                userDefaults.setBool(sunriseNotificationsSwitch.on, forKey: SuntrackerUserDefaultsConstants.SunriseNotificationsUserDefaultsKey)
            }
            else if sender == sunsetNotificationSwitch {
                userDefaults.setBool(sunsetNotificationSwitch.on, forKey: SuntrackerUserDefaultsConstants.SunsetNotificationsUserDefaultsKey)
            }
            
            if sunriseNotificationsSwitch.on || sunsetNotificationSwitch.on {
                scheduleLocalNotifications()
            }
        }
    }
    
    // MARK: Settings Alert
    private func displayPermissionsAlert() {
        
        let alertController = UIAlertController(title: "Permissions", message: "Changing notification settings must be done in the Settings App", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let settingsAction = UIAlertAction(title: "Open Settings App", style: .Default) {
            action in
            
            if let url = NSURL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alertController.addAction(settingsAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }

    // MARK: Local notifications
    private func scheduleLocalNotifications() {
        
        if allowNotificationsSwitch.on {
            if let operationCoordinator = operationCoordinator,
                let location = location {
                    operationCoordinator.createLocalNotifications(location.coordinate, sunriseLocalNotifications: sunriseNotificationsSwitch.on, sunsetLocalNotifications: sunsetNotificationSwitch.on, application: UIApplication.sharedApplication())
            }
        }
    }
}
