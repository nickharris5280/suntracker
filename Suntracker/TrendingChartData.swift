//
//  TrendingChartData.swift
//  Suntracker
//
//  Created by Nick Harris on 2/29/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit

/**
 Holds calculated information for the trending chart used to both draw the chart as well as to display when a particular part of the chart is touched.
 
 - totalMinutes: the total minutes of sun time which is used to draw the chart
 - dateString: the date string to display when the bar of the chart is touched
 - sunriseTime: the sunrise time to display when the bar of the chart is touched
 - sunsetTime: the sunset time to display when the bar of the chart is touched
 - totalDaylight: the total daylight string to display when the bar of the chart is touched
 */
struct TrendingChartData {

    let totalMinutes: Double
    let dateString: String
    let sunriseTime: String
    let sunsetTime: String
    let totalDaylight: String
    
    init(totalMinutes: Double, dateString: String, sunriseTime: String, sunsetTime: String, totalDaylight: String) {
        self.totalMinutes = totalMinutes
        self.dateString = dateString
        self.sunriseTime = sunriseTime
        self.sunsetTime = sunsetTime
        self.totalDaylight = totalDaylight
    }
}
