//
//  String+Extensions.swift
//  Suntracker
//
//  Created by Nick Harris on 3/6/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import Foundation

extension String {
    
    /**
     Creates a string in the form of "___ hrs ___ mins" calculated from the sunrise and sunset times represented as minutes from midnight.
     
     - Parameter sunriseTimeInMinutes: sunrise time represented in minutes from midnight
     - Parameter sunsetTimeInMinutes: sunset time represented in minutes from midnight
     */
    static func totalDaylightString(sunriseTimeInMinutes: Double, sunsetTimeInMinutes: Double) -> String {
        
        let totalMinutes = sunsetTimeInMinutes - sunriseTimeInMinutes
        let totalHours = Int(totalMinutes / 60)
        let remainderMinutes = Int(totalMinutes - Double(totalHours * 60))
        
        let totalDaylightString = "\(totalHours) hrs \(remainderMinutes) mins"
        return totalDaylightString
    }
    
    /**
     Creates a string in the form of "hh:mm a" calculated from the number of minutes from midnight.
     
     - Parameter minutes: the number minutes from minight
     - Parameter isDST: is daylight savings time in effect
     */
    static func timeStringFromMinutes(minutes: Double, isDST: Bool) -> String {
        
        let floatHour = minutes / 60.0
        var hour = floor(floatHour)
        let floatMinute = 60.0 * (floatHour - floor(floatHour))
        var minute = floor(floatMinute)
        let floatSec = 60.0 * (floatMinute - floor(floatMinute))
        let second = floor(floatSec + 0.5)
        
        let addSecond = (second >= 30) ? 1 : 0
        minute = minute + Double(addSecond)
        
        if minute >= 60 {
            minute = minute - 60
            hour = hour + 1
        }
        
        if hour > 23 {
            hour = hour - 24
        }
        else if hour < 0 {
            hour = hour + 24
        }
        
        if isDST {
            hour = hour + 1
        }
        
        var PM = false
        if hour > 12 {
            hour = hour - 12
            PM = true
        }
        else if hour == 12 {
            PM = true
        }
        else if hour == 0 {
            PM = false
            hour = 12
        }
        
        let amPm = PM == true ? " PM" : " AM"
        var timeStr = String(Int(hour)) + ":"
        if minute < 10 {
            timeStr = timeStr + "0" + String(Int(minute)) + amPm
        }
        else {
            timeStr = timeStr + String(Int(minute)) + amPm
        }
        
        return timeStr
    }
}
