//
//  SuntrackerConstants.swift
//  Suntracker
//
//  Created by Nick Harris on 2/19/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

/**
Trending chart type

- ThreeMonth: three month chart meaning 1.5 months into the past and 1.5 months into the future from today
- SixMonth: six month chart meaning 3 months into the past and 3 months into the future from today
- Year: year chart meaning 6 months into the past and 6 months into the future from today
*/
enum TrendingChartType: Int {
    case ThreeMonth = 0
    case SixMonth = 1
    case Year = 2
    
    /**
     The number of days to go back and into the future depending on the chart type. For example .Year = 182 which is floor(365/2).
     */
    func daysFromCurrentDate() -> Int {
        switch self {
        case .ThreeMonth: return 46
        case .SixMonth: return 96
        case .Year: return 182
        }
    }
}

/**
 Local notification type
 
 - Unknown: not set - used in operation init functions
 - Sunrise: sunrise notification
 - Sunset: sunset notification
 */
enum LocalNotificationType {
    case Unknown
    case Sunrise
    case Sunset
    
    /**
     Title used when displaying the location notifications
     */
    func alertTitle() -> String {

        switch self {
        case .Unknown: fatalError("LocationNotificationType is unknown")
        case .Sunrise: return "Sunrise"
        case .Sunset: return "Sunset"
        }
    }
    
    /**
     Body text used when displaying a notification
     */
    func alertBody(timeString: String) -> String {
        
        switch self {
        case .Unknown: fatalError("LocationNotificationType is unknown")
        case .Sunrise: return "Good Morning!\nSunrise: \(timeString)"
        case .Sunset: return "Sunset: \(timeString)\nEnjoy your evening."
        }
    }
}

/**
User Default Constants
 
 - UserDefaultsSuiteName: Suite name used to share user defaults between the app and the today extension
 - SunriseTimeUserDefaultsKey: Last saved calculated sunrise time string
 - SunsetTimeUserDefaultsKey: Last saved calculated sunset time string
 - TotalDaylightUserDefaultsKey: Last saved calculated total daylight string
 - DaysUntilDSTSwitchUserDefaultsKey: Last saved calculated days to DST switch string
 - LastLocalityUserDefaultsKey: Last saved locality string
 - LastDayCalculatedUserDefaultsKey: Last saved calculated day
 - SunriseNotificationsUserDefaultsKey: Bool value - true if sunrise notifications are enabled
 - SunsetNotificationsUserDefaultsKey: Bool value - true if sunset notifications are enabled
 - LastLocationFetchUserDefaultsKey: Date of the last location fetch
 - LastStringsOperationUserDefaultsKey: Date of the last calculated strings for both the app and today extension labels
 */
struct SuntrackerUserDefaultsConstants {
    static let UserDefaultsSuiteName = "group.cliftongarage.suntracker"
    static let SunriseTimeUserDefaultsKey = "SunriseTimeUserDefaultsKey"
    static let SunsetTimeUserDefaultsKey = "SunsetTimeUserDefaultsKey"
    static let TotalDaylightUserDefaultsKey = "TotalDaylightUserDefaultsKey"
    static let DaysUntilDSTSwitchUserDefaultsKey = "DaysUntilDSTSwitchUserDefaultsKey"
    static let LastLocalityUserDefaultsKey = "LastLocalityUserDefaultsKey"
    static let LastDayCalculatedUserDefaultsKey = "LastDayCalculatedUserDefaultsKey"
    
    static let SunriseNotificationsUserDefaultsKey = "SunriseNotificationsUserDefaultsKey"
    static let SunsetNotificationsUserDefaultsKey = "SunsetNotificationsUserDefaultsKey"
    
    static let LastLocationFetchUserDefaultsKey = "LastLocationFetchUserDefaultsKey"
    static let LastStringsOperationUserDefaultsKey = "LastStringsOperationUserDefaultsKey"
    
    static let SelectedTrendChartUserDefaultKey = "SelectedTrendChartUserDefaultKey"
}

/**
Numeric Constants

 - FifteenMilesInMeters: Fifteen miles converted to its meter value
 - TotalMinutesInDate: Number of minutes in a day (24 * 60)
 - SecondsInHour: Number of seconds in an hour (60 * 60)
*/
struct SuntrackerNumericConstants {
    
    static let FifteenMilesInMeters = 24140.2
    static let TotalMinutesInDay = 1440.0
    static let SecondsInHour = 3600.0
}