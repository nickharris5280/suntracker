//
//  BarGraphView.swift
//  Suntracker
//
//  Created by Nick Harris on 2/28/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit

/**
A UIView subclass used to create the bar graph. It holds information used to both draw itself in the graph as well as to be displayed when touched.
 
 - trendingChartData: The data used to draw the chart as well as to show when this particular bar is in focus by the user
 */
class TrendingChartBarView: UIView {

    var trendingChartData: TrendingChartData?

}
