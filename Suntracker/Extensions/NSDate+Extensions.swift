//
//  NSDate+Extensions.swift
//  Suntracker
//
//  Created by Nick Harris on 2/17/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import Foundation

extension NSDate {
    
    /**
     Returns true if the NSDate is in the future.
     */
    func isInFuture() -> Bool {
        return Double(self.timeIntervalSinceNow) > 0.0
    }
    
    /**
     Creates a new NSDate that is N number of days ahead or behind the given date.
     
     - Parameter days: The number of days (positive or negative) away from the given NSDate to use when creating the new NSDate
     */
    func newDateByDays(days : Int) -> NSDate {
        let dateComponents = NSDateComponents()
        dateComponents.day = days
        return NSCalendar.currentCalendar().dateByAddingComponents(dateComponents, toDate: self, options: NSCalendarOptions(rawValue: 0))!
    }
    
    /**
     The current day of the year in the range of 1 to 366
     */
    static func dayOfYear() -> Int {
        
        let cal = NSCalendar.currentCalendar()
        let dayOfYear = cal.ordinalityOfUnit(.Day, inUnit: .Year, forDate: NSDate())
        
        return dayOfYear
    }
}